import { bot, db } from './common/init.js';
import { formatSpaces, getSpentMoney } from "./common/helpers.js";

bot.onText(/\/spent (.+)/, (msg, match) => {
    const chatId = msg.chat.id;
    const cost = match[1].split(' ')[0];
    if(!/^\d+$/.test(cost)) {
        bot.sendMessage(chatId, 'Неверный формат.\nПример: /spent 500 сосисоны');
        return;
    }
    const name = match[1].slice(cost.length + 1);
    db.prepare('INSERT INTO spending_history (name, cost, chat_id) VALUES (?, ?, ?)').run(name, cost, chatId);
    bot.sendMessage(chatId, `Расходы записаны. Потрачено уже ${getSpentMoney(chatId)}.`);
});

bot.onText(/\/history/, (msg) => {
    const chatId = msg.chat.id;
    let rows = db.prepare('SELECT name, cost, created_at FROM spending_history WHERE chat_id = ?').all(chatId);
    rows = rows.map(row => `${row.created_at.slice(5)}: ${row.name} - ${formatSpaces(row.cost)}.`);
    if (rows.length > 0) bot.sendMessage(msg.chat.id, rows.join('\n'));
    bot.sendMessage(chatId, `Всего потрачено: ${getSpentMoney(chatId)}.`);
});

bot.onText(/\/clear/, (msg) => {
    const chatId = msg.chat.id;
    db.prepare('DELETE FROM spending_history WHERE chat_id = ?').run(chatId);
    bot.sendMessage(chatId, `История расходов удалена.`);
});




