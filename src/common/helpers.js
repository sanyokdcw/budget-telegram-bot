import {db} from "./init.js";

export const formatSpaces = number => number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ' тг';

export const getSpentMoney = chatId => {
    const rows = db.prepare('SELECT * FROM spending_history WHERE chat_id = ?').all(chatId);
    let sum = 0;
    rows.forEach(row => {
        sum += row.cost;
    });
    return formatSpaces(sum);
}
